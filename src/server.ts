import express from 'express'
import helmet from 'helmet'
import cors from 'cors'
import logger from './logger/logger'

const PORT = process.env.PORT ?? 3000

const app = express()

app.set('trust proxy', true)

app.use(helmet())
app.use(cors())
app.use(express.json({ limit: '10kb' }))
app.use(express.urlencoded({ extended: false }))

app.get('/health', (_, res) => {
  res.status(200).send('All is well!')
})

app.listen(PORT, async () => {
  logger.log(`App listening at http://localhost:${PORT}`)
})
